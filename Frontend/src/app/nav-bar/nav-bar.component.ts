import { Component, OnInit } from '@angular/core';
import { AlertifyService } from '../services/alertify.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  loggedinUser : string | null;
  constructor(private alertifyService: AlertifyService) { }

  ngOnInit() {
    console.log(localStorage.getItem('token'));
    console.log(localStorage.getItem('token')!==null);
  }

  loggedIn()
  {
    this.loggedinUser = localStorage.getItem('token');
    return this.loggedinUser;
  }

  onLogout(){
    localStorage.removeItem('token');
    this.alertifyService.success('Successfully Logged out');
  }
}
