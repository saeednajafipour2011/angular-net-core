import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup,AbstractControl, ValidatorFn, Validators } from '@angular/forms';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/services/user.service';
import { AlertifyService } from 'src/app/services/alertify.service';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {
  registerationForm : FormGroup;
  user: User;
  userSubmitted: boolean;
  constructor(private userService: UserService, private alertifyService:AlertifyService) { }

  passwordMatchingValidator(): ValidatorFn { 
    return (control: AbstractControl): { [key: string]: any } | null =>  
    control.get('password')!.value === control.get('confirmPassword')!.value ? null : {notMatched:true};
  }
  ngOnInit() {
    this.registerationForm = new FormGroup({
      userName : new FormControl('Mark',Validators.required),
      email: new FormControl(null,[Validators.email,Validators.required]),
      password: new FormControl(null,[Validators.minLength(8),Validators.required]),
      confirmPassword: new FormControl(null,[Validators.required]),
      mobile: new FormControl(null,[Validators.maxLength(11),Validators.required])
    }, this.passwordMatchingValidator());
  }

  // Getter methods

  get userName(){
    return this.registerationForm.get('userName') as FormControl;
  }

  get email(){
    return this.registerationForm.get('email') as FormControl;
  }

  get password(){
    return this.registerationForm.get('password') as FormControl;
  }

  get confirmPassword(){
    return this.registerationForm.get('confirmPassword') as FormControl;
  }

  get mobile(){
    return this.registerationForm.get('mobile') as FormControl;
  }

  userData() : User
  {
      return this.user={
        userName: this.userName.value,
        password: this.password.value,
        mobile: this.mobile.value,
        email: this.email.value
      };
  }

  onSubmit(){
    console.log(this.registerationForm);
    this.userSubmitted=true;
    if(this.registerationForm.valid){
      //this.user=Object.assign(this.user,this.registerationForm.value);
      this.user=this.userData();
      this.userService.addUser(this.user);
      this.registerationForm.reset();
      this.userSubmitted=false;
      this.alertifyService.success("Congrats, you are successfully registered");
    }
    else{
      this.alertifyService.error("Kindly provide the required fields!");
    }
  }

}
