import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Property } from '../model/property';
import { IPropertyBase } from '../model/ipropertybase';


@Injectable({
  providedIn: 'root'
})
export class HousingService {

  constructor(private http:HttpClient) { }

  getAllProperties(SellRent?: number) : Observable<Property[]>
  {
    return this.http.get('data/properties.json').pipe(
      map(
        
        data=>{
          var propertiesArray: Array<Property> = [];
          for(let i=0;i<Object.values(data).length;i++)
          {
            if(data.hasOwnProperty(i)){
              var item=Object.values(data)[i];
              if(SellRent)
              {
                if(item.SellRent==SellRent)
                {
                  var ip = {} as Property;
                  ip.id=item.Id;
                  ip.sellRent=item.SellRent;
                  ip.name=item.Name;
                  ip.propertyType=item.PType;
                  ip.furnishingType=item.FType;
                  ip.price=item.Price;
                  ip.bhk=item.BHK;
                  ip.builtArea=item.BuiltArea;
                  ip.city=item.City;
                  ip.readyToMove=item.RTM;
                  ip.photo=item.Image;
                  ip.estPossessionOn=item.Posession;
                  ip.carpetArea=item.CarpetArea;
                  ip.address=item.Address+', '+item.Address2+', '+item.Address3;
                  ip.address2=item.Address2;
                  ip.floorNo=item.FloorNo;
                  ip.totalFloors=item.TotalFloor;
                  ip.age=item.AOP;
                  ip.maintenance=item.MainEntrance;
                  ip.gated=item.Gated;
                  ip.maintenance=item.Maintenance;
                  ip.description=item.Description;
  
                  propertiesArray.push(ip);
                }
              }
              else{
                var ip = {} as Property;
                  ip.id=item.Id;
                  ip.sellRent=item.SellRent;
                  ip.name=item.Name;
                  ip.propertyType=item.PType;
                  ip.furnishingType=item.FType;
                  ip.price=item.Price;
                  ip.bhk=item.BHK;
                  ip.builtArea=item.BuiltArea;
                  ip.city=item.City;
                  ip.readyToMove=item.RTM;
                  ip.photo=item.Image;
                  ip.estPossessionOn=item.Posession;
                  ip.carpetArea=item.CarpetArea;
                  ip.address=item.Address+', '+item.Address2+', '+item.Address3;
                  ip.address2=item.Address2;
                  ip.floorNo=item.FloorNo;
                  ip.totalFloors=item.TotalFloor;
                  ip.age=item.AOP;
                  ip.maintenance=item.MainEntrance;
                  ip.gated=item.Gated;
                  ip.maintenance=item.Maintenance;
                  ip.description=item.Description;
  
                  propertiesArray.push(ip);
              }
              
            }
            
          }

          const newProperty= JSON.parse(localStorage.getItem('newProp') || '{}');

          for(let i=0;i<newProperty.length;i++)
          {
            if(SellRent)
            {
              if(newProperty && SellRent==newProperty[i].sellRent)
              {
                propertiesArray=[newProperty[i],...propertiesArray];
              }
            }
            else{
              if(newProperty)
              {
                propertiesArray=[newProperty[i],...propertiesArray];
              }
            }
          }

          return propertiesArray;
        }
      )
    );
  }

  addProperty(property: Property){
    let properties=[];
    if(localStorage.getItem('newProp'))
    {
      properties=JSON.parse(localStorage.getItem('newProp') || '{}');
      properties=[property,...properties];
    }
    else{
      properties=[property];
    } 

    localStorage.setItem('newProp',JSON.stringify(properties));
  }

  newPropID(): number
  {
    if(localStorage.getItem('PID'))
    {
      localStorage.setItem('PID',String(parseInt(localStorage.getItem('PID') || '101')+1));
      return parseInt(localStorage.getItem('PID') || '101');
    }
    else{
      localStorage.setItem('PID','101');
      return parseInt(localStorage.getItem('PID') || '101');
    }
  }

  
  getProperty(id: number) {
    return this.getAllProperties().pipe(
        map( propertiesArray => {
          //throw new Error('Some error!');
          return propertiesArray.find(p => p.id===id);
        }
      )
    );
  }

}
