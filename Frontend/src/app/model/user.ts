export interface User {
    userName: string;
    password: string;
    mobile: string;
    email: string;
}
