import { IPropertyBase } from './ipropertybase';

export interface IProperty extends IPropertyBase{
    Desscription: string | null;
}