import { IPropertyBase } from './ipropertybase';
import { Photo } from './photo';

export class Property implements IPropertyBase {
    id?: number|null;
    sellRent?: number|null;
    name?: string|null;
    propertyTypeId?: number|null;
    propertyType?: string|null;
    bhk?: number|null;
    furnishingTypeId?: number|null;
    furnishingType?: string|null;
    price?: number|null;
    builtArea?: number|null;
    carpetArea?: number|null;
    address?: string|null;
    address2?: string|null;
    CityId?: number|null;
    city?: string|null;
    floorNo?: string|null;
    totalFloors?: string|null;
    readyToMove?: boolean|null;
    age?: string|null;
    mainEntrance?: string|null;
    security?: number|null;
    gated?: boolean|null;
    maintenance?: number|null;
    estPossessionOn?: string|null;
    photo?: string|null;
    description?: string|null;
    photos?: Photo[]|null;
    postedOn?: string;
}
