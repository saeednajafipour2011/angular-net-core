import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Property } from 'src/app/model/property';
import { HousingService } from 'src/app/services/housing.service';
import { Gallery, GalleryRef } from 'ng-gallery';

@Component({
  selector: 'app-property-detail',
  templateUrl: './property-detail.component.html',
  styleUrls: ['./property-detail.component.css'],
  template: `
  <gallery [items]="images"></gallery>
`
})
export class PropertyDetailComponent implements OnInit {


  property = new Property(); 
  galleryId = 'mixedExample';

  public propertyId:number=0;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private housingService: HousingService,
    private gallery: Gallery) {   }

  ngOnInit() {
    this.propertyId=Number(this.route.snapshot.params['id']);

    // using the resolver
    this.route.data.subscribe(
      (data: Property) => {
        this.property=data['prp'];
      }
    );

    const galleryRef: GalleryRef = this.gallery.ref(this.galleryId);

    galleryRef.addImage({
      src: 'assets/images/house_1.jpg',
      thumb: 'assets/images/house_1.jpg',
      title: 'Some title'
    });

    galleryRef.addImage({
      src: 'assets/images/house_2.jpg',
      thumb: 'assets/images/house_1.jpg',
      title: 'Some title'
    });

    galleryRef.addImage({
      src: 'assets/images/house_3.jpg',
      thumb: 'assets/images/house_1.jpg',
      title: 'Some title'
    });

    // galleryRef.addVideo({
    //   src: 'VIDEO_URL',
    //   thumb: '(OPTIONAL)VIDEO_THUMBNAIL_URL',
    //   poster: '(OPTIONAL)VIDEO_POSTER_URL'
    // });

    // // Add a video item with multiple url sources
    // galleryRef.addVideo({
    //   src: [
    //     { url: 'MP4_URL', type: 'video/mp4' },
    //     { url: 'OGG_URL', type: 'video/ogg' }
    //   ],
    //   thumb: '(OPTIONAL)VIDEO_THUMBNAIL_URL',
    //   poster: '(OPTIONAL)VIDEO_POSTER_URL'
    // });

    // galleryRef.addYoutube({
    //   src: 'VIDEO_ID'
    // });

    // galleryRef.addIframe({
    //   src: 'IFRAME_URL',
    //   thumb: '(OPTIONAL)IMAGE_THUMBNAIL_URL'
    // });

    // using directly without the resolver
    // this.route.params.subscribe(
    //   (params)=> {
    //     this.propertyId=+ params['id'];
    //     this.housingService.getProperty(this.propertyId).subscribe(
    //       (data: Property) => {
    //         this.property = data;
    //       }, Error => this.router.navigate(['/'])
          
    //     );
    //   }
    // );
  }


}
