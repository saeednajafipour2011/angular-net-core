import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPropertyBase } from 'src/app/model/ipropertybase';
import { Property } from 'src/app/model/property';
import { HousingService } from 'src/app/services/housing.service';


@Component({
  selector: 'app-property-list',
  templateUrl: './property-list.component.html',
  styleUrls: ['./property-list.component.css']
})
export class PropertyListComponent implements OnInit 
{
  SellRent = 1;
  properties: Array<IPropertyBase>;
  Today= new Date();
  City= '';
  SortedByParam= '';
  searchCity= '';
  SortDirection='asc';
  

  constructor(private housingService: HousingService, private activatedRout: ActivatedRoute) { 
    this.properties=[];
  }

  ngOnInit(): void {
    if (this.activatedRout.snapshot.url.toString())
    {
      this.SellRent=2;
    }
    this.housingService.getAllProperties(this.SellRent).subscribe(
      data=>{
        this.properties=data;
      },
      error=>{
        console.log("http error:");
        console.log(error);
      }
    );
  }

  onCityFilter(){
    this.searchCity=this.City;
  }

  onCityFilterClear(){
    this.searchCity= '';
  }
  onSortDirection(){
    if(this.SortDirection === 'desc')
    {
      this.SortDirection='asc';
    }
    else{
      this.SortDirection='desc';
    }
  }

}
