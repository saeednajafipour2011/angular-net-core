import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { IPropertyBase } from 'src/app/model/ipropertybase';
import { Property } from 'src/app/model/property';
import { AlertifyService } from 'src/app/services/alertify.service';
import { HousingService } from 'src/app/services/housing.service';

@Component({
  selector: 'app-add-property',
  templateUrl: './add-property.component.html',
  styleUrls: ['./add-property.component.css']
})
export class AddPropertyComponent implements OnInit {

  // template driven
  //@ViewChild('Form') addPropertyForm : NgForm;

  @ViewChild('formTabs') formTabs?: TabsetComponent;

  // reactive from

  addPropertyForm: FormGroup;

  property = new Property();

  // Will come from masters
  propertyType : Array<string>=['House','Apartment','Duplex'];
  furnishType : Array<string>=['Fully','Semi','Unfurnished'];
  nextClicked: boolean=false;

  propertyView: IPropertyBase = {
    id: null,
    name: '',
    price: null,
    sellRent: null,
    propertyType: null,
    furnishingType: null,
    bhk: null,
    builtArea: null,
    city: '',
    readyToMove: null
};



  constructor(
    private alertify:AlertifyService,
    private fb: FormBuilder,
    private router:Router, 
    private housingService:HousingService, 
    private datePipe: DatePipe) { }

  ngOnInit() {
    this.CreateAddPropertyFrom();
  }

  CreateAddPropertyFrom(){
    this.addPropertyForm = new FormGroup({
      BasicInfo: new FormGroup({
        SellRent: new FormControl('1',Validators.required),
        BHK: new FormControl(null,Validators.required),
        PType: new FormControl(null,Validators.required),
        FType: new FormControl(null,Validators.required),
        Name: new FormControl(null,Validators.required),
        City: new FormControl(null,Validators.required)
      }),
      PriceInfo: new FormGroup({
        Price: new FormControl(null,Validators.required),
        BuiltArea: new FormControl(null,Validators.required),
        CarpetArea: new FormControl(null),
        Security: new FormControl(null),
        Maintenance: new FormControl(null)
      }),
      AddressInfo: new FormGroup({
        FloorNo: new FormControl(null),
        TotalFloor: new FormControl(null),
        Address: new FormControl(null,Validators.required),
        LandMark: new FormControl(null)
      }),
      OtherInfo: new FormGroup({
        RTM: new FormControl(null,Validators.required),
        PossessionOn: new FormControl(null,Validators.required),
        AOP: new FormControl(null),
        Gated: new FormControl(null),
        MainEntrance: new FormControl(null),
        Description: new FormControl(null)
      })
    });
  }

  //#region < Getter Methods >
    //#region < Form Groups >
    get BasicInfo(){
      return this.addPropertyForm.controls['BasicInfo'] as FormGroup;
    }
  
    get PriceInfo(){
      return this.addPropertyForm.controls['PriceInfo'] as FormGroup;
    }
  
    get AddressInfo(){
      return this.addPropertyForm.controls['AddressInfo'] as FormGroup;
    }
  
    get OtherInfo(){
      return this.addPropertyForm.controls['OtherInfo'] as FormGroup;
    }
    //#endregion

    //#region < Form Controls >

    get SellRent() {
      return this.BasicInfo.controls['SellRent'] as FormControl;
  }

  get BHK() {
      return this.BasicInfo.controls['BHK'] as FormControl;
  }

  get PType() {
      return this.BasicInfo.controls['PType'] as FormControl;
  }

  get FType() {
      return this.BasicInfo.controls['FType'] as FormControl;
  }

  get Name() {
      return this.BasicInfo.controls['Name'] as FormControl;
  }

  get City() {
      return this.BasicInfo.controls['City'] as FormControl;
  }

  get Price() {
      return this.PriceInfo.controls['Price'] as FormControl;
  }

  get BuiltArea() {
      return this.PriceInfo.controls['BuiltArea'] as FormControl;
  }

  get CarpetArea() {
      return this.PriceInfo.controls['CarpetArea'] as FormControl;
  }

  get Security() {
      return this.PriceInfo.controls['Security'] as FormControl;
  }

  get Maintenance() {
      return this.PriceInfo.controls['Maintenance'] as FormControl;
  }

  get FloorNo() {
      return this.AddressInfo.controls['FloorNo'] as FormControl;
  }

  get TotalFloor() {
      return this.AddressInfo.controls['TotalFloor'] as FormControl;
  }

  get Address() {
      return this.AddressInfo.controls['Address'] as FormControl;
  }

  get LandMark() {
      return this.AddressInfo.controls['LandMark'] as FormControl;
  }

  get RTM() {
      return this.OtherInfo.controls['RTM'] as FormControl;
  }

  get PossessionOn() {
      return this.OtherInfo.controls['PossessionOn'] as FormControl;
  }

  get AOP() {
      return this.OtherInfo.controls['AOP'] as FormControl;
  }

  get Gated() {
      return this.OtherInfo.controls['Gated'] as FormControl;
  }

  get MainEntrance() {
      return this.OtherInfo.controls['MainEntrance'] as FormControl;
  }

  get Description() {
      return this.OtherInfo.controls['Description'] as FormControl;
  }

  
    //#endregion
  //#endregion


  mapProperty(): void {
    this.property.id = this.housingService.newPropID();
    this.property.sellRent = +this.SellRent.value;
    this.property.bhk = this.BHK.value;
    this.property.propertyTypeId = this.PType.value;
    this.property.name = this.Name.value;
    this.property.city = this.City.value;
    this.property.furnishingTypeId = this.FType.value;
    this.property.price = this.Price.value;
    this.property.security = this.Security.value;
    this.property.maintenance = this.Maintenance.value;
    this.property.builtArea = this.BuiltArea.value;
    this.property.carpetArea = this.CarpetArea.value;
    this.property.floorNo = this.FloorNo.value;
    this.property.totalFloors = this.TotalFloor.value;
    this.property.address = this.Address.value;
    this.property.address2 = this.LandMark.value;
    this.property.readyToMove = this.RTM.value;
    this.property.gated = this.Gated.value;
    this.property.mainEntrance = this.MainEntrance.value;
    this.property.estPossessionOn = this.datePipe.transform(this.PossessionOn.value,'MM/dd/yyyy');
    this.property.description = this.Description.value;
    this.property.postedOn= new Date().toDateString();
}

  onBack(){
    this.router.navigate(['/']);
  }

  onSubmit()
  {
    this.nextClicked=true;
    
    if(this.allTabsValid())
    {
      this.mapProperty();
      this.housingService.addProperty(this.property);
      this.alertify.success('Congrats, form submitted!');
    }
    else{
      this.alertify.error('Please review the form and provide valid enteries!');
    }
    
    console.log(this.addPropertyForm);

    if(this.SellRent.value==='2')
    {
      this.router.navigate(['/rent=property']);
    }
    else
    {
      this.router.navigate(['/']);
    }
  }

  // onSubmit(Form:NgForm)
  // {
  //   console.log('Congrats, form submitted!');
  //   console.log(Form);
  // }

  allTabsValid(): boolean{
    if (this.BasicInfo.invalid){
      this.formTabs.tabs[0].active = true;
      return false;
    }

    if (this.PriceInfo.invalid){
      this.formTabs.tabs[1].active = true;
      return false;
    }

    if (this.AddressInfo.invalid){
      this.formTabs.tabs[2].active = true;
      return false;
    }

    if (this.OtherInfo.invalid){
      this.formTabs.tabs[3].active = true;
      return false;
    }
    return true;

  }

  selectTab(tabId: number, isCurrentTabValid: boolean) {
    this.nextClicked=true;
    if (this.formTabs?.tabs[tabId] && isCurrentTabValid) {
      this.formTabs.tabs[tabId].active = true;
    }
    else if (this.formTabs?.tabs[tabId] && !isCurrentTabValid)
    {
      this.alertify.error("Please check your inputs!");
    }
  }

  selectTabSimple(tabId: number) {
    this.nextClicked=true;
    if (this.formTabs?.tabs[tabId]) {
      this.formTabs.tabs[tabId].active = true;
    }
  }
}
