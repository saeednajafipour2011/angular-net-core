using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
//using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        public CityController()
        {
        }

        [HttpGet("")]
        public IEnumerable<string> Get()
        {
            return new string[] {"Atlanta","New York","Chicago","Boston"};
        }
        [HttpGet("{id}")]
        public string Get(int id)
        {
            string[] cities= {"Atlanta","New York","Chicago","Boston"};
            return cities[id];
        }    
    }
}